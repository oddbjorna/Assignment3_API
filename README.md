# Assignment 3 - Web API
For this assignment we were to make an API using ASP.NET Core.
The API revolves around movies, franchises and characters.

The Assignment consists of the following:

* A database made with Entity Framework Core using a SQL Server with a RESTFUL API.
* Web API using ASP.NET Core

## Appendix A
Buisness rules:

* A movie can contain many characters, and a character can star in multiple movies.
* A movie belongs to one franchise, but a franchise can contain many movies.

The database must be seeded with some dummy data. We added 4 movies, 4 characters and 2 franchises.

## Appendix B

API Requirements:

*Updating related data*

* Add a character to an existing movie
* Remove a character from an existing movie
* Add a movie to an existing franchise
* Remove a movie from an existing franchise

*Reporting*

* Get all movies in a franchise
* Get all the characters in a movie
* Get all the characters in a franchise

We made DTO (Data Transfer Objects) for all the model classes clients will interract with.

## Contributers

* Obbjørn Almenning
* Sunniva Stolt-Nielsen
* Nanfrid Idsø
