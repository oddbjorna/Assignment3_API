﻿using Assignment3_API.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3_API.Models
{
    public class MoviesDbContext : DbContext
    {
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Character> Characters { get; set; }
        public DbSet<Franchise> Franchises { get; set; }

        public MoviesDbContext([NotNullAttribute] DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Seed data
            modelBuilder.Entity<Character>().HasData(new Character { Id = 1, FullName = "Fili", Alias = "Dean O'Gorman", Gender = "Male", Picture = new Uri("https://static.wikia.nocookie.net/heroes-and-villain/images/8/8c/Profile_-_Fili.jpg/revision/latest/top-crop/width/360/height/450?cb=20190808025134") });
            modelBuilder.Entity<Character>().HasData(new Character { Id = 2, FullName = "Kili", Alias = "Aidan Turner", Gender = "Male", Picture = new Uri("https://static.wikia.nocookie.net/middlearthfilmseries/images/8/86/KiliHOBBIT.png/revision/latest?cb=20180227145403") });
            modelBuilder.Entity<Character>().HasData(new Character { Id = 3, FullName = "Oin", Alias = "John Callen", Gender = "Male", Picture = new Uri("https://static.wikia.nocookie.net/lotr/images/e/e8/Oin.jpg/revision/latest?cb=20120908125247") });
            modelBuilder.Entity<Character>().HasData(new Character { Id = 4, FullName = "Katniss Everdeen", Alias = "Mockingjay", Gender = "Female", Picture = new Uri("https://theoddapple.com/wp-content/uploads/2016/01/katniss-everdeen-the-hunger-games-red-outfit.jpg") });

            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 1, MovieTitle = "An Unexpected Journey", ReleaseYear = DateTime.Parse("2012-11-28"), FranchiseId = 1, Genre = "Fantasy", Director = "Peter Jackson", Trailer = new Uri("https://www.youtube.com/watch?v=nOGsB9dORBg&ab_channel=CieonMovies"), Picture = new Uri("https://i-viaplay-com.akamaized.net/viaplay-prod/149/116/1460152199-06527256670701da9be51dbd8e30c50b57028371.jpg?width=400&height=600") });
            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 2, MovieTitle = "The Desolation of Smaug", ReleaseYear = DateTime.Parse("2013-12-11"), FranchiseId = 1, Genre = "Fantasy", Director = "Peter Jackson", Trailer = new Uri("https://www.youtube.com/watch?v=OPVWy1tFXuc&ab_channel=WarnerBros.UK%26Ireland"), Picture = new Uri("https://m.media-amazon.com/images/M/MV5BMzU0NDY0NDEzNV5BMl5BanBnXkFtZTgwOTIxNDU1MDE@._V1_.jpg") });
            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 3, MovieTitle = "The Battle of the Five Armies", ReleaseYear = DateTime.Parse("2014-12-20"), FranchiseId = 1, Genre = "Fantasy", Director = "Peter Jackson", Trailer = new Uri("https://www.youtube.com/watch?v=ZSzeFFsKEt4&ab_channel=WarnerBros.Pictures"), Picture = new Uri("https://static.posters.cz/image/750/plakater/the-hobbit-3-battle-of-five-armies-smaug-i21722.jpg") });
            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 4, MovieTitle = "The Hunger Games", ReleaseYear = DateTime.Parse("2012-03-24"), FranchiseId = 2, Genre = "Adventure, Sci-Fi", Director = "Gary Ross", Trailer = new Uri("https://www.youtube.com/watch?v=mfmrPu43DF8&ab_channel=LionsgateMovies"), Picture = new Uri("https://flxt.tmsimg.com/assets/p8648012_p_v10_ab.jpg") });

            modelBuilder.Entity<Franchise>().HasData(new Franchise() { Id = 1, Name = "The Hobbit", Description = "The Hobbit is a film series consisting of three high fantasy adventure films directed by Peter Jackson. " +
                "The three films are The Hobbit: An Unexpected Journey (2012), The Hobbit: The Desolation of Smaug (2013), and The Hobbit: The Battle of the Five Armies (2014)." +
                "The films are based on the 1937 novel The Hobbit by J. R. R. Tolkien, with large portions of the trilogy inspired by the appendices to The Return of the King, which expand on the story told in The Hobbit, as well as new material and characters written especially for the films. " +
                "Together they act as a prequel to Jackson's The Lord of the Rings film trilogy." });
            modelBuilder.Entity<Franchise>().HasData(new Franchise() { Id = 2, Name = "The Hunger Games", Description = "The Hunger Games film series is composed of science fiction dystopian adventure films, based on The Hunger Games trilogy of novels by the American author Suzanne Collins. " +
                "The movies are distributed by Lionsgate and produced by Nina Jacobson and Jon Kilik. The series feature an ensemble cast including Jennifer Lawrence as Katniss Everdeen, Josh Hutcherson as Peeta Mellark, Liam Hemsworth as Gale Hawthorne, Woody Harrelson as Haymitch Abernathy, Elizabeth Banks as Effie Trinket, Stanley Tucci as Caesar Flickerman, and Donald Sutherland as President Snow." });

            modelBuilder.Entity<Movie>()
                .HasMany(p => p.Characters)
                .WithMany(m => m.Movies)
                .UsingEntity<Dictionary<string, object>>(
                    "MovieCharacter",
                    r => r.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                    l => l.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                    je =>
                    {
                        je.HasKey("MovieId", "CharacterId");
                        je.HasData(
                            new { MovieId = 1, CharacterId = 1 },
                            new { MovieId = 1, CharacterId = 2 },
                            new { MovieId = 1, CharacterId = 3 },
                            new { MovieId = 2, CharacterId = 1 },
                            new { MovieId = 2, CharacterId = 2 },
                            new { MovieId = 2, CharacterId = 3 },
                            new { MovieId = 3, CharacterId = 1 },
                            new { MovieId = 3, CharacterId = 2 },
                            new { MovieId = 3, CharacterId = 3 },
                            new { MovieId = 4, CharacterId = 4 }
                        );
                    });
        }
    }
}
