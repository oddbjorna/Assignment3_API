﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3_API.Models.DTO.Movie
{
    public class MovieCreateDTO
    {
        public string MovieTitle { get; set; }
        public string Genre { get; set; }
        public DateTime ReleaseYear { get; set; }
        public string Director { get; set; }
        public Uri Picture { get; set; }
        public Uri Trailer { get; set; }
        public int? FranchiseId { get; set; }
    }
}
