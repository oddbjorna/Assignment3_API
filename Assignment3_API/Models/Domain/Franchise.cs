﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3_API.Models.Domain
{
    [Table("Franchise")]
    public class Franchise
    {
        // Fields
        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        [MaxLength(999)]
        public string Description { get; set; }

        // Relationships
        public ICollection<Movie> Movies { get; set; }

    }
}
