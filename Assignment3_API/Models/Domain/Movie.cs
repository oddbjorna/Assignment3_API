﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3_API.Models.Domain
{
    [Table("Movie")]
    public class Movie
    {
        // Fields
        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string MovieTitle { get; set; }

        [MaxLength(50)]
        public string Genre { get; set; }

        [Required]
        public DateTime ReleaseYear { get; set; }
        public string Director { get; set; }
        public Uri Picture { get; set; }
        public Uri Trailer { get; set; }

        // Relationships
        public ICollection<Character> Characters { get; set; }
        public int? FranchiseId { get; set; }
    }
}
