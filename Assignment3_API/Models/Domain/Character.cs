﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3_API.Models.Domain
{
    [Table("Character")]
    public class Character
    {
        // Fields
        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string FullName { get; set; }

        [MaxLength(50)]
        public string Alias { get; set; }
        public string Gender { get; set; }
        public Uri Picture { get; set; }

        // Relationships
        public ICollection<Movie> Movies { get; set; }
    }
}
