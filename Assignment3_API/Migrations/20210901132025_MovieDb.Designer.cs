﻿// <auto-generated />
using System;
using Assignment3_API.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Assignment3_API.Migrations
{
    [DbContext(typeof(MoviesDbContext))]
    [Migration("20210901132025_MovieDb")]
    partial class MovieDb
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.9")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Assignment3_API.Models.Domain.Character", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Alias")
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("FullName")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("Gender")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Picture")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Character");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Alias = "Dean O'Gorman",
                            FullName = "Fili",
                            Gender = "Male",
                            Picture = "https://static.wikia.nocookie.net/heroes-and-villain/images/8/8c/Profile_-_Fili.jpg/revision/latest/top-crop/width/360/height/450?cb=20190808025134"
                        },
                        new
                        {
                            Id = 2,
                            Alias = "Aidan Turner",
                            FullName = "Kili",
                            Gender = "Male",
                            Picture = "https://static.wikia.nocookie.net/middlearthfilmseries/images/8/86/KiliHOBBIT.png/revision/latest?cb=20180227145403"
                        },
                        new
                        {
                            Id = 3,
                            Alias = "John Callen",
                            FullName = "Oin",
                            Gender = "Male",
                            Picture = "https://static.wikia.nocookie.net/lotr/images/e/e8/Oin.jpg/revision/latest?cb=20120908125247"
                        },
                        new
                        {
                            Id = 4,
                            Alias = "Mockingjay",
                            FullName = "Katniss Everdeen",
                            Gender = "Female",
                            Picture = "https://theoddapple.com/wp-content/uploads/2016/01/katniss-everdeen-the-hunger-games-red-outfit.jpg"
                        });
                });

            modelBuilder.Entity("Assignment3_API.Models.Domain.Franchise", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Description")
                        .HasMaxLength(999)
                        .HasColumnType("nvarchar(999)");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.HasKey("Id");

                    b.ToTable("Franchise");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Description = "The Hobbit is a film series consisting of three high fantasy adventure films directed by Peter Jackson. The three films are The Hobbit: An Unexpected Journey (2012), The Hobbit: The Desolation of Smaug (2013), and The Hobbit: The Battle of the Five Armies (2014).The films are based on the 1937 novel The Hobbit by J. R. R. Tolkien, with large portions of the trilogy inspired by the appendices to The Return of the King, which expand on the story told in The Hobbit, as well as new material and characters written especially for the films. Together they act as a prequel to Jackson's The Lord of the Rings film trilogy.",
                            Name = "The Hobbit"
                        },
                        new
                        {
                            Id = 2,
                            Description = "The Hunger Games film series is composed of science fiction dystopian adventure films, based on The Hunger Games trilogy of novels by the American author Suzanne Collins. The movies are distributed by Lionsgate and produced by Nina Jacobson and Jon Kilik. The series feature an ensemble cast including Jennifer Lawrence as Katniss Everdeen, Josh Hutcherson as Peeta Mellark, Liam Hemsworth as Gale Hawthorne, Woody Harrelson as Haymitch Abernathy, Elizabeth Banks as Effie Trinket, Stanley Tucci as Caesar Flickerman, and Donald Sutherland as President Snow.",
                            Name = "The Hunger Games"
                        });
                });

            modelBuilder.Entity("Assignment3_API.Models.Domain.Movie", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Director")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int?>("FranchiseId")
                        .HasColumnType("int");

                    b.Property<string>("Genre")
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("MovieTitle")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("Picture")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("ReleaseYear")
                        .HasColumnType("datetime2");

                    b.Property<string>("Trailer")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("FranchiseId");

                    b.ToTable("Movie");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Director = "Peter Jackson",
                            FranchiseId = 1,
                            Genre = "Fantasy",
                            MovieTitle = "An Unexpected Journey",
                            Picture = "https://i-viaplay-com.akamaized.net/viaplay-prod/149/116/1460152199-06527256670701da9be51dbd8e30c50b57028371.jpg?width=400&height=600",
                            ReleaseYear = new DateTime(2012, 11, 28, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Trailer = "https://www.youtube.com/watch?v=nOGsB9dORBg&ab_channel=CieonMovies"
                        },
                        new
                        {
                            Id = 2,
                            Director = "Peter Jackson",
                            FranchiseId = 1,
                            Genre = "Fantasy",
                            MovieTitle = "The Desolation of Smaug",
                            Picture = "https://m.media-amazon.com/images/M/MV5BMzU0NDY0NDEzNV5BMl5BanBnXkFtZTgwOTIxNDU1MDE@._V1_.jpg",
                            ReleaseYear = new DateTime(2013, 12, 11, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Trailer = "https://www.youtube.com/watch?v=OPVWy1tFXuc&ab_channel=WarnerBros.UK%26Ireland"
                        },
                        new
                        {
                            Id = 3,
                            Director = "Peter Jackson",
                            FranchiseId = 1,
                            Genre = "Fantasy",
                            MovieTitle = "The Battle of the Five Armies",
                            Picture = "https://static.posters.cz/image/750/plakater/the-hobbit-3-battle-of-five-armies-smaug-i21722.jpg",
                            ReleaseYear = new DateTime(2014, 12, 20, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Trailer = "https://www.youtube.com/watch?v=ZSzeFFsKEt4&ab_channel=WarnerBros.Pictures"
                        },
                        new
                        {
                            Id = 4,
                            Director = "Gary Ross",
                            FranchiseId = 2,
                            Genre = "Adventure, Sci-Fi",
                            MovieTitle = "The Hunger Games",
                            Picture = "https://flxt.tmsimg.com/assets/p8648012_p_v10_ab.jpg",
                            ReleaseYear = new DateTime(2012, 3, 24, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Trailer = "https://www.youtube.com/watch?v=mfmrPu43DF8&ab_channel=LionsgateMovies"
                        });
                });

            modelBuilder.Entity("MovieCharacter", b =>
                {
                    b.Property<int>("MovieId")
                        .HasColumnType("int");

                    b.Property<int>("CharacterId")
                        .HasColumnType("int");

                    b.HasKey("MovieId", "CharacterId");

                    b.HasIndex("CharacterId");

                    b.ToTable("MovieCharacter");

                    b.HasData(
                        new
                        {
                            MovieId = 1,
                            CharacterId = 1
                        },
                        new
                        {
                            MovieId = 1,
                            CharacterId = 2
                        },
                        new
                        {
                            MovieId = 1,
                            CharacterId = 3
                        },
                        new
                        {
                            MovieId = 2,
                            CharacterId = 1
                        },
                        new
                        {
                            MovieId = 2,
                            CharacterId = 2
                        },
                        new
                        {
                            MovieId = 2,
                            CharacterId = 3
                        },
                        new
                        {
                            MovieId = 3,
                            CharacterId = 1
                        },
                        new
                        {
                            MovieId = 3,
                            CharacterId = 2
                        },
                        new
                        {
                            MovieId = 3,
                            CharacterId = 3
                        },
                        new
                        {
                            MovieId = 4,
                            CharacterId = 4
                        });
                });

            modelBuilder.Entity("Assignment3_API.Models.Domain.Movie", b =>
                {
                    b.HasOne("Assignment3_API.Models.Domain.Franchise", null)
                        .WithMany("Movies")
                        .HasForeignKey("FranchiseId");
                });

            modelBuilder.Entity("MovieCharacter", b =>
                {
                    b.HasOne("Assignment3_API.Models.Domain.Character", null)
                        .WithMany()
                        .HasForeignKey("CharacterId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Assignment3_API.Models.Domain.Movie", null)
                        .WithMany()
                        .HasForeignKey("MovieId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Assignment3_API.Models.Domain.Franchise", b =>
                {
                    b.Navigation("Movies");
                });
#pragma warning restore 612, 618
        }
    }
}
