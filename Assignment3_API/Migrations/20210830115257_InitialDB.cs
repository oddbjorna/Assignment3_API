﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Assignment3_API.Migrations
{
    public partial class InitialDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Character",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FullName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Alias = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Gender = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Picture = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Character", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Franchise",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(999)", maxLength: 999, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Franchise", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Movie",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MovieTitle = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Genre = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    ReleaseYear = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Director = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Picture = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Trailer = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FranchiseId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movie", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Movie_Franchise_FranchiseId",
                        column: x => x.FranchiseId,
                        principalTable: "Franchise",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MovieCharacter",
                columns: table => new
                {
                    MovieId = table.Column<int>(type: "int", nullable: false),
                    CharacterId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MovieCharacter", x => new { x.MovieId, x.CharacterId });
                    table.ForeignKey(
                        name: "FK_MovieCharacter_Character_CharacterId",
                        column: x => x.CharacterId,
                        principalTable: "Character",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MovieCharacter_Movie_MovieId",
                        column: x => x.MovieId,
                        principalTable: "Movie",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Character",
                columns: new[] { "Id", "Alias", "FullName", "Gender", "Picture" },
                values: new object[,]
                {
                    { 1, "Dean O'Gorman", "Fili", "Male", "https://static.wikia.nocookie.net/heroes-and-villain/images/8/8c/Profile_-_Fili.jpg/revision/latest/top-crop/width/360/height/450?cb=20190808025134" },
                    { 2, "Aidan Turner", "Kili", "Male", "https://static.wikia.nocookie.net/middlearthfilmseries/images/8/86/KiliHOBBIT.png/revision/latest?cb=20180227145403" },
                    { 3, "John Callen", "Oin", "Male", "https://static.wikia.nocookie.net/lotr/images/e/e8/Oin.jpg/revision/latest?cb=20120908125247" },
                    { 4, "Mockingjay", "Katniss Everdeen", "Female", "https://theoddapple.com/wp-content/uploads/2016/01/katniss-everdeen-the-hunger-games-red-outfit.jpg" }
                });

            migrationBuilder.InsertData(
                table: "Franchise",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "The Hobbit is a film series consisting of three high fantasy adventure films directed by Peter Jackson. The three films are The Hobbit: An Unexpected Journey (2012), The Hobbit: The Desolation of Smaug (2013), and The Hobbit: The Battle of the Five Armies (2014).The films are based on the 1937 novel The Hobbit by J. R. R. Tolkien, with large portions of the trilogy inspired by the appendices to The Return of the King, which expand on the story told in The Hobbit, as well as new material and characters written especially for the films. Together they act as a prequel to Jackson's The Lord of the Rings film trilogy.", "The Hobbit" },
                    { 2, "The Hunger Games film series is composed of science fiction dystopian adventure films, based on The Hunger Games trilogy of novels by the American author Suzanne Collins. The movies are distributed by Lionsgate and produced by Nina Jacobson and Jon Kilik. The series feature an ensemble cast including Jennifer Lawrence as Katniss Everdeen, Josh Hutcherson as Peeta Mellark, Liam Hemsworth as Gale Hawthorne, Woody Harrelson as Haymitch Abernathy, Elizabeth Banks as Effie Trinket, Stanley Tucci as Caesar Flickerman, and Donald Sutherland as President Snow.", "The Hunger Games" }
                });

            migrationBuilder.InsertData(
                table: "Movie",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "MovieTitle", "Picture", "ReleaseYear", "Trailer" },
                values: new object[,]
                {
                    { 1, "Peter Jackson", 1, "Fantasy", "An Unexpected Journey", "https://i-viaplay-com.akamaized.net/viaplay-prod/149/116/1460152199-06527256670701da9be51dbd8e30c50b57028371.jpg?width=400&height=600", new DateTime(2012, 11, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), "https://www.youtube.com/watch?v=nOGsB9dORBg&ab_channel=CieonMovies" },
                    { 2, "Peter Jackson", 1, "Fantasy", "The Desolation of Smaug", "https://m.media-amazon.com/images/M/MV5BMzU0NDY0NDEzNV5BMl5BanBnXkFtZTgwOTIxNDU1MDE@._V1_.jpg", new DateTime(2013, 12, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), "https://www.youtube.com/watch?v=OPVWy1tFXuc&ab_channel=WarnerBros.UK%26Ireland" },
                    { 3, "Peter Jackson", 1, "Fantasy", "The Battle of the Five Armies", "https://static.posters.cz/image/750/plakater/the-hobbit-3-battle-of-five-armies-smaug-i21722.jpg", new DateTime(2014, 12, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), "https://www.youtube.com/watch?v=ZSzeFFsKEt4&ab_channel=WarnerBros.Pictures" },
                    { 4, "Gary Ross", 2, "Adventure, Sci-Fi", "The Hunger Games", "https://flxt.tmsimg.com/assets/p8648012_p_v10_ab.jpg", new DateTime(2012, 3, 24, 0, 0, 0, 0, DateTimeKind.Unspecified), "https://www.youtube.com/watch?v=mfmrPu43DF8&ab_channel=LionsgateMovies" }
                });

            migrationBuilder.InsertData(
                table: "MovieCharacter",
                columns: new[] { "CharacterId", "MovieId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 2, 1 },
                    { 3, 1 },
                    { 1, 2 },
                    { 2, 2 },
                    { 3, 2 },
                    { 1, 3 },
                    { 2, 3 },
                    { 3, 3 },
                    { 4, 4 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Movie_FranchiseId",
                table: "Movie",
                column: "FranchiseId");

            migrationBuilder.CreateIndex(
                name: "IX_MovieCharacter_CharacterId",
                table: "MovieCharacter",
                column: "CharacterId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MovieCharacter");

            migrationBuilder.DropTable(
                name: "Character");

            migrationBuilder.DropTable(
                name: "Movie");

            migrationBuilder.DropTable(
                name: "Franchise");
        }
    }
}
