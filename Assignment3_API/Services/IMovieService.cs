﻿using Assignment3_API.Models.Domain;
using Assignment3_API.Models.DTO.Character;
using Assignment3_API.Models.DTO.Movie;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3_API.Services
{
    public interface IMovieService
    {
        public Task<IEnumerable<Movie>> GetMoviesAsync();
        public Task<Movie> GetMovieAsync(int id);
        public Task PutMovieAsync(Movie movie);
        public Task PostMovieAsync(Movie movie);
        public Task DeleteMovieAsync(Movie movie);
        public Task DeleteCharacterFromMovieAsync(Movie movie, Character character);
        public bool MovieExists(int id);

    }
}
