﻿using Assignment3_API.Models.Domain;
using Assignment3_API.Models.DTO.Character;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3_API.Services
{
    public interface ICharacterService
    {
        public Task<IEnumerable<Character>> GetCharactersAsync();
        public Task<Character> GetCharacterAsync(int id);
        public Task PutCharacterAsync(Character character);
        public Task PostCharacterAsync(Character character);
        public Task DeleteCharacter(Character character);
        public bool CharacterExists(int id);
    }
}
