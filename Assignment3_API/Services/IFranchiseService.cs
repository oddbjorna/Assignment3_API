﻿using Assignment3_API.Models.Domain;
using Assignment3_API.Models.DTO.Character;
using Assignment3_API.Models.DTO.Franchise;
using Assignment3_API.Models.DTO.Movie;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3_API.Services
{
    public interface IFranchiseService
    {
        public Task<IEnumerable<Franchise>> GetFranchisesAsync();
        public Task<Franchise> GetFranchiseAsync(int id);
        public Task PutFranchiseAsync(Franchise franchise);

        public Task PostFranchiseAsync(Franchise franchise);

        public Task DeleteFranchiseAsync(Franchise franchise);
        public Task DeleteMovieFromFranchiseAsync(Franchise franchise, Movie movie);
        public bool FranchiseExists(int id);
    }
}
