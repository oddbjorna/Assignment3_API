﻿using Assignment3_API.Models;
using Assignment3_API.Models.Domain;
using Assignment3_API.Models.DTO.Franchise;
using Assignment3_API.Models.DTO.Character;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3_API.Services
{
    public class FranchiseService : IFranchiseService
    {
        private readonly MoviesDbContext _context;

        public FranchiseService(MoviesDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Franchise>> GetFranchisesAsync()
        {
            return await _context.Franchises.Include(f => f.Movies).ToListAsync();
        }

        public async Task<Franchise> GetFranchiseAsync(int id)
        {
            return await _context.Franchises.Include(f => f.Movies).Where(f => f.Id == id).FirstAsync();
        }

        public async Task PutFranchiseAsync(Franchise franchise)
        {
            _context.Entry(franchise).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task PostFranchiseAsync(Franchise franchise)
        {
            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();

        }

        public async Task DeleteFranchiseAsync(Franchise franchise)
        {
            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteMovieFromFranchiseAsync(Franchise franchise, Movie movie)
        {
            franchise.Movies.Remove(movie);
            await _context.SaveChangesAsync();
        }
        public bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(e => e.Id == id);
        }
    }
}
