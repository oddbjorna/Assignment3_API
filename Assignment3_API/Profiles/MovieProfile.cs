﻿using Assignment3_API.Models.Domain;
using Assignment3_API.Models.DTO.Movie;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3_API.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, MovieReadDTO>()
            .ForMember(cdto => cdto.Characters, opt => opt
            .MapFrom(c => c.Characters.Select(c => c.Id).ToArray()))
            .ReverseMap();

            CreateMap<MovieCreateDTO, Movie>();

            CreateMap<MovieEditDTO, Movie>();
        }
    }
}
