﻿using Assignment3_API.Models.DTO.Franchise;
using Assignment3_API.Models.Domain;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3_API.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseReadDTO>()
            .ForMember(fdto => fdto.Movies, opt => opt
            .MapFrom(c => c.Movies.Select(c => c.Id).ToArray()))
            .ReverseMap();

            CreateMap<FranchiseCreateDTO, Franchise>();

            CreateMap<FranchiseEditDTO, Franchise>();
        }
    }
}
