﻿using Assignment3_API.Models.Domain;
using Assignment3_API.Models.DTO.Character;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3_API.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<Character, CharacterReadDTO>()
            .ForMember(cdto => cdto.Movies, opt => opt
            .MapFrom(c => c.Movies.Select(c => c.Id).ToArray()))
            .ReverseMap();

            CreateMap<CharacterCreateDTO, Character>();

            CreateMap<CharacterEditDTO, Character>();
        }
        
    }
}
