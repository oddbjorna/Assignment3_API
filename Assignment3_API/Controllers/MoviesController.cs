﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment3_API.Models;
using Assignment3_API.Models.Domain;
using System.Net.Mime;
using AutoMapper;
using Assignment3_API.Models.DTO.Movie;
using Assignment3_API.Models.DTO.Character;
using Assignment3_API.Services;

namespace Assignment3_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class MoviesController : ControllerBase
    {
        private readonly IFranchiseService _franchiseService;
        private readonly IMovieService _movieService;
        private readonly ICharacterService _characterService;
        private readonly IMapper _mapper;

        public MoviesController(IFranchiseService franchiseService, IMovieService movieService, ICharacterService characterService, IMapper mapper)
        {
            _franchiseService = franchiseService;
            _movieService = movieService;
            _characterService = characterService;
            _mapper = mapper;
        }

        // GET: api/Movies
        /// <summary>
        /// Gets all movies from the database.
        /// </summary>
        /// <returns>Returns a list of movies.</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies()
        {
            return _mapper.Map<List<MovieReadDTO>>(await _movieService.GetMoviesAsync());
        }

        /// <summary>
        /// Gets all the characters from a movie Id provided by parameters.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns a list of characters on success, 404 Not Found on fail.</returns>
        [HttpGet("get-characters/{id}")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetAllCharactersFromMovie(int id)
        {
            var movie = _mapper.Map<MovieReadDTO>(await _movieService.GetMovieAsync(id));

            if (movie == null)
            {
                return NotFound();
            }

            List<CharacterReadDTO> characters = new();

            var domainMovie = await _movieService.GetMovieAsync(movie.Id);
            foreach (Character character in domainMovie.Characters)
            {
                var newCharacter = _mapper.Map<CharacterReadDTO>(await _characterService.GetCharacterAsync(character.Id));
                characters.Add(newCharacter);
            }

            return characters;

        }

        // GET: api/Movies/5
        /// <summary>
        /// Gets a specific movie based on Id provided by parameter.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns a movie on success, 404 Not Found on fail.</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovie(int id)
        {
            var movie = _mapper.Map<MovieReadDTO>(await _movieService.GetMovieAsync(id));

            if (movie == null)
            {
                return NotFound();
            }

            return movie;
        }

        // PUT: api/Movies/5
        /// <summary>
        /// Updates a specific movie in the database based on id provided by parameters.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movie"></param>
        /// <returns>Returns 204 No Content on success, 400 Bad Request or 404 Not Found on fail.</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, MovieEditDTO movie)
        {
            if (id != movie.Id)
            {
                return BadRequest();
            }
            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }

            Movie domainMovie = _mapper.Map<Movie>(movie);

            await _movieService.PutMovieAsync(domainMovie);
          

            return NoContent();
        }

        /// <summary>
        /// Adds characters to a specific movie based on id provided by parameters.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="array"></param>
        /// <returns>Returns 204 No Content on success, 404 Not Found on fail.</returns>
        [HttpPut("addcharacters/{id}")]
        public async Task<IActionResult> AddCharacters(int id, int[] array)
        {
            Movie movie = await _movieService.GetMovieAsync(id);

            if (movie == null)
            {
                return NotFound();
            }

            foreach (int charNumber in array)
            {
                var character = await _characterService.GetCharacterAsync(charNumber);
                if (character != null)
                {
                    movie.Characters.Add(character);
                }
            }

            await _movieService.PutMovieAsync(movie);

            return NoContent();
        }


        // POST: api/Movies
        /// <summary>
        /// Creates a new movie in the database.
        /// </summary>
        /// <param name="movie"></param>
        /// <returns>Returns 201 Created Movie.</returns>
        [HttpPost]
        public async Task<ActionResult<Movie>> PostMovie(MovieCreateDTO movie)
        {
            Movie domainMovie = _mapper.Map<Movie>(movie);

            await _movieService.PostMovieAsync(domainMovie);

            return CreatedAtAction("GetMovie", new { id = domainMovie.Id }, domainMovie);
        }

        // DELETE: api/Movies/5
        /// <summary>
        /// Deletes a movie from the database.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns 204 No Content on success, 404 Not Found on fail.</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            var movie = await _movieService.GetMovieAsync(id);
            if (movie == null)
            {
                return NotFound();
            }

            await _movieService.DeleteMovieAsync(movie);

            return NoContent();
        }

        /// <summary>
        /// Removes a character from a movie.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="charId"></param>
        /// <returns>Returns 204 No Content on success, 404 Not Found on fail.</returns>
        [HttpDelete("remove-character/{id}")]
        public async Task<IActionResult> DeleteCharacterFromMovie(int id, int charId)
        {
            Character character = await _characterService.GetCharacterAsync(charId);
            Movie movie = await _movieService.GetMovieAsync(id);

            if (movie == null)
            {
                return NotFound();
            }
            await _movieService.DeleteCharacterFromMovieAsync(movie, character);


            return NoContent();
        }
    }
}
