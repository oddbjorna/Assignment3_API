﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment3_API.Models;
using Assignment3_API.Models.Domain;
using System.Net.Mime;
using AutoMapper;
using Assignment3_API.Models.DTO.Franchise;
using Assignment3_API.Models.DTO.Character;
using Assignment3_API.Models.DTO.Movie;
using Assignment3_API.Services;

namespace Assignment3_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class FranchisesController : ControllerBase
    {
        private readonly IFranchiseService _franchiseService;
        private readonly IMovieService _movieService;
        private readonly ICharacterService _characterService;
        private readonly IMapper _mapper;

        public FranchisesController(IMovieService movieService, ICharacterService characterService, IFranchiseService franchiseService, IMapper mapper)
        {
            _franchiseService = franchiseService;
            _movieService = movieService;
            _characterService = characterService;
            _mapper = mapper;
        }

        // GET: api/Franchises
        /// <summary>
        /// Gets all the franchise from the database.
        /// </summary>
        /// <returns>Returns a list of franchises.</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises()
        {
            return _mapper.Map<List<FranchiseReadDTO>>(await _franchiseService.GetFranchisesAsync());
        }

        /// <summary>
        /// Gets all movies from a franchise Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns a list of movies on success, 404 Not Found on fail.</returns>
        [HttpGet("get-movies/{id}")]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetAllMoviesFromFranchise(int id)
        {
            var franchise = _mapper.Map<FranchiseReadDTO>(await _franchiseService.GetFranchiseAsync(id));

            if (franchise == null)
            {
                return NotFound();
            }

            List<MovieReadDTO> movies = new();

            var domainFranchise = await _franchiseService.GetFranchiseAsync(franchise.Id);
            foreach (Movie movie in domainFranchise.Movies)
            {
                var newMovie = _mapper.Map<MovieReadDTO>(await _movieService.GetMovieAsync(movie.Id));
                movies.Add(newMovie);
            }

            return movies;
        }

        /// <summary>
        /// Gets all the characters from all the movies within a franchise.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns a list of characters on success, 404 Not found on fail</returns>
        [HttpGet("get-characters/{id}")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetAllCharactersFromFranchise(int id)
        {
            var franchise = _mapper.Map<FranchiseReadDTO>(await _franchiseService.GetFranchiseAsync(id));

            if (franchise == null)
            {
                return NotFound();
            }

            List<CharacterReadDTO> characters = new();
            List<Character> temp = new();

            foreach (int movie in franchise.Movies)
            {
                var newMovie = await _movieService.GetMovieAsync(movie);
                if (newMovie != null)
                {
                    foreach (Character character in newMovie.Characters)
                    {
                        if (!temp.Contains(character))
                        {
                            var newCharacter = _mapper.Map<CharacterReadDTO>(await _characterService.GetCharacterAsync(character.Id));
                            characters.Add(newCharacter);
                            temp.Add(character);
                        }
                    }
                }
            }

            return characters;

        }

        // GET: api/Franchises/5
        /// <summary>
        /// Gets a specific franchise based on ID provided in parameters.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns a franchise on success, 404 Not Found on fail.</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchise(int id)
        {
            var franchise = _mapper.Map<FranchiseReadDTO>(await _franchiseService.GetFranchiseAsync(id));

            if (franchise == null)
            {
                return NotFound();
            }

            return franchise;
        }

        // PUT: api/Franchises/5
        /// <summary>
        /// Updates a franchise in the database.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="franchise"></param>
        /// <returns>Returns 204 No content on success, 400 Bad Request or 404 Not found on fail.</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, FranchiseEditDTO franchise)
        {
            if (id != franchise.Id)
            {
                return BadRequest();
            }
            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }
            Franchise domainFranchise = _mapper.Map<Franchise>(franchise);
            await _franchiseService.PutFranchiseAsync(domainFranchise);
   
            return NoContent();
        }

        /// <summary>
        /// Adds movies to a franchise.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="array"></param>
        /// <returns>Returns 204 No Content on success, 404 Not Found on fail.</returns>
        [HttpPut("addmovies/{id}")]
        public async Task<IActionResult> AddMovies(int id, int[] array)
        {
            Franchise franchise = await _franchiseService.GetFranchiseAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }

            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }

            foreach (int movieNumber in array)
            {
                var movie = await _movieService.GetMovieAsync(movieNumber);
                if (movie != null)
                {
                    franchise.Movies.Add(movie);
                }
            }

            await _franchiseService.PutFranchiseAsync(franchise);

            return NoContent();
        }

        // POST: api/Franchises
        /// <summary>
        /// Creates a new franchise in the database.
        /// </summary>
        /// <param name="franchise"></param>
        /// <returns>Returns 201 Created Franchise.</returns>
        [HttpPost]
        public async Task<ActionResult<Franchise>> PostFranchise(FranchiseCreateDTO franchise)
        {
            Franchise domainFranchise = _mapper.Map<Franchise>(franchise);
            await _franchiseService.PostFranchiseAsync(domainFranchise);

            return CreatedAtAction("GetFranchise", new { id = domainFranchise.Id }, domainFranchise);
        }

        // DELETE: api/Franchises/5
        /// <summary>
        /// Deletes a franchise from the database.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns 204 No Content on success, 404 Not Found on fail.</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            var franchise = await _franchiseService.GetFranchiseAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }

            franchise.Movies.Select(m => m.FranchiseId = null);

            await _franchiseService.DeleteFranchiseAsync(franchise);

            return NoContent();
        }

        /// <summary>
        /// Removes a movie from a franchise.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movieId"></param>
        /// <returns>Returns 204 No Content on success, 404 Not Found on fail.</returns>
        [HttpDelete("remove-movie/{id}")]
        public async Task<IActionResult> DeleteMovieFromFranchise(int id, int movieId)
        {
            Movie movie = await _movieService.GetMovieAsync(movieId);
            Franchise franchise = await _franchiseService.GetFranchiseAsync(id);

            if (franchise == null)
            {
                return NotFound();
            }

            await _franchiseService.DeleteMovieFromFranchiseAsync(franchise, movie);

            return NoContent();
        }
    }
}
