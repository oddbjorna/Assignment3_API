﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment3_API.Models;
using Assignment3_API.Models.Domain;
using Assignment3_API.Models.DTO.Character;
using System.Net.Mime;
using AutoMapper;
using Assignment3_API.Services;

namespace Assignment3_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class CharactersController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ICharacterService _characterService;

        public CharactersController(IMapper mapper, ICharacterService characterService)
        {
            _mapper = mapper;
            _characterService = characterService;
        }

        // GET: api/Characters
        /// <summary>
        /// Gets all the characters from the database.
        /// </summary>
        /// <returns>Returns a list of characters</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacters()
        {
            return _mapper.Map<List<CharacterReadDTO>>(await _characterService.GetCharactersAsync());
        }

        // GET: api/Characters/5
        /// <summary>
        /// Gets a specific character based on id provided by a parameter.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns CharacterReadDTP on success, 404 Not found on fail.</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacter(int id)
        {
            var character = _mapper.Map<CharacterReadDTO>(await _characterService.GetCharacterAsync(id));

            if (character == null)
            {
                return NotFound();
            }

            return character;
        }

        // PUT: api/Characters/5
        /// <summary>
        /// Updates the attributes of an existing character based on id provided by a parameter.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="character"></param>
        /// <returns>Returns 204 No Content on success, 400 Bad Request or 404 Not Found on fail.</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, CharacterEditDTO character)
        {
            if (id != character.Id)
            {
                return BadRequest();
            }
            if (!_characterService.CharacterExists(id))
            {
                return NotFound();
            }
            Character domainCharacter = _mapper.Map<Character>(character);

            await _characterService.PutCharacterAsync(domainCharacter);

            return NoContent();
        }

        // POST: api/Characters
        /// <summary>
        /// Creates a new character in the database.
        /// </summary>
        /// <param name="character"></param>
        /// <returns>Returns 201 Created Character</returns>
        [HttpPost]
        public async Task<ActionResult<Character>> PostCharacter(CharacterCreateDTO character)
        {
            Character domainCharacter = _mapper.Map<Character>(character);
            await _characterService.PostCharacterAsync(domainCharacter);

            return CreatedAtAction("GetCharacter", new { id = domainCharacter.Id }, domainCharacter);
        }

        // DELETE: api/Characters/5
        /// <summary>
        /// Deletes a character from the database based on id from parameters.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns 204 No Content, returns 404 Not Found on fail.</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            var character = await _characterService.GetCharacterAsync(id);
            if (character == null)
            {
                return NotFound();
            }

            await _characterService.DeleteCharacter(character);

            return NoContent();
        }
    }
}
